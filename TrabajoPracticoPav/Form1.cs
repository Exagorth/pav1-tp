﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrabajoPracticoPav
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void limpiarCampos()
        {
            txtNombre.Text = "";
            txtContraseña.Text = "";
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text.Equals("") || txtContraseña.Text.Equals(""))
            {
                MessageBox.Show("Por favor no deje campos vacíos....");
            }
            else
            {
                string nombreUsuario = "";
                nombreUsuario = txtNombre.Text;
                DialogResult respuestaLogin = MessageBox.Show($"¿Desea logearse como {nombreUsuario}?","Confirmar Acceso",MessageBoxButtons.OKCancel,MessageBoxIcon.Exclamation);
                
                if (respuestaLogin == DialogResult.OK)
                {
                    MessageBox.Show($"Bienvenido {txtNombre.Text}!");
                    limpiarCampos();
                }
                else
                {
                    MessageBox.Show("Complete los campos nuevamente");
                    limpiarCampos();
                }
            }
        }

        private void txtMensaje_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //buscar metodo "DISPOSE"
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
